package com.moviles.kotlinmartialarts.viewmodel

import androidx.lifecycle.ViewModel
import com.moviles.kotlinmartialarts.model.network.FirestoreServiceSingleton

class UserViewModel : ViewModel() {

    fun add_user(firstName: String, lastName: String, email: String, username:String , password: String, Gender:String, city:String, state:String, country:String)
    {
        FirestoreServiceSingleton.getInstance().add_user(firstName,lastName,email,username,password,Gender,city,state,country)
    }

}