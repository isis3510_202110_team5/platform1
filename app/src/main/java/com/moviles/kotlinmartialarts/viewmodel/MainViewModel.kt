package com.moviles.kotlinmartialarts.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.DocumentReference
import com.moviles.kotlinmartialarts.model.Message
import com.moviles.kotlinmartialarts.model.User
import com.moviles.kotlinmartialarts.model.network.FirebaseCallback
import com.moviles.kotlinmartialarts.model.network.FirestoreServiceSingleton


class MainViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    private val listMessages: MutableLiveData<List<Message>> = MutableLiveData()
    fun getMessages(channel:DocumentReference) : LiveData<List<Message>> {
        FirestoreServiceSingleton.getInstance().getMessages(channel,object:
            FirebaseCallback<List<Message>> {

            override fun onFailed(exception: Exception) {
                TODO("Not yet implemented")
            }

            override fun onSuccess(result: List<Message>?) {
                listMessages.postValue(result)
            }

        })

        return listMessages
    }

    fun getUser(id:String) : LiveData<User> {
        val user: MutableLiveData<User> = MutableLiveData()
        FirestoreServiceSingleton.getInstance().getUser(id,object:
                FirebaseCallback<User> {

            override fun onFailed(exception: Exception) {
                TODO("Not yet implemented")
            }

            override fun onSuccess(result: User?) {
                user.postValue(result)            }

        })

        return user
    }

    fun getUserMain(id:String) : LiveData<User> {
        val userM: MutableLiveData<User> = MutableLiveData()
        FirestoreServiceSingleton.getInstance().getUser(id,object:
                FirebaseCallback<User> {

            override fun onFailed(exception: Exception) {
                TODO("Not yet implemented")
            }

            override fun onSuccess(result: User?) {
                userM.postValue(result)            }

        })

        return userM
    }



    fun setMessages(message: Message) {
        FirestoreServiceSingleton.getInstance().setMessages(message)
        FirestoreServiceSingleton.getInstance().setChannelMessage(message.channel)

    }
    private val idChannel: MutableLiveData<Array<Any>> = MutableLiveData()

    fun getChannel(name: String): LiveData<Array<Any>> {
        FirestoreServiceSingleton.getInstance().getIdChannel(name,object:
                        FirebaseCallback<Array<Any>>{
                    override fun onFailed(exception: Exception) {

                        TODO("Not yet implemented")
                    }

            override fun onSuccess(result: Array<Any>?) {
                idChannel.postValue(result)
            }

        })
        return idChannel

    }
    private val idUser: MutableLiveData<Array<Any>> = MutableLiveData()


    fun document(reference: String): DocumentReference {
        val currentDocRef: DocumentReference =
                FirestoreServiceSingleton.getInstance().getReference(reference)
        return currentDocRef

    }
}