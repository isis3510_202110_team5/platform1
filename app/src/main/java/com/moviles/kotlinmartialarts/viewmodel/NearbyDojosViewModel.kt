package com.moviles.kotlinmartialarts.viewmodel

import android.location.Location
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.google.firebase.firestore.GeoPoint
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.model.Dojo
import com.moviles.kotlinmartialarts.model.network.FirebaseCallback
import com.moviles.kotlinmartialarts.model.network.FirestoreServiceSingleton
import com.moviles.kotlinmartialarts.model.repository.DataRepositorySingleton
import com.moviles.kotlinmartialarts.view.adapter.RecyclerNearbyDojosAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.pow
import kotlin.math.sqrt

class NearbyDojosViewModel : ViewModel() {

    private val listDojos: MutableLiveData<List<Dojo>> = MutableLiveData()
    private val userLocation: LiveData<Location> =
        DataRepositorySingleton.getInstance().getLocation()

    private var recyclerNearbyDojosAdapter: RecyclerNearbyDojosAdapter? = null

    fun getRecyclerNearbyDojosAdapter(): RecyclerNearbyDojosAdapter? {
        recyclerNearbyDojosAdapter = RecyclerNearbyDojosAdapter(this, R.layout.item_nearby_dojo)
        return recyclerNearbyDojosAdapter
    }

    fun setDojosInRecyclerAdapter(itemCount: Int) {
        recyclerNearbyDojosAdapter?.itemCount = itemCount
        recyclerNearbyDojosAdapter?.notifyDataSetChanged()
    }

    fun getUserLocation(): LiveData<Location> {
        return userLocation
    }

    fun setUserLocation(newLocation: Location) {
        DataRepositorySingleton.getInstance().setLocation(newLocation)
    }

    fun getDojoAt(position: Int): Dojo? {
        return listDojos.value?.get(position)
    }

    fun getDojos(): LiveData<List<Dojo>> {
        if (listDojos.value.isNullOrEmpty()) {
            FirestoreServiceSingleton.getInstance().getDojos(object : FirebaseCallback<List<Dojo>> {
                override fun onSuccess(result: List<Dojo>?) {
                    if (result != null) {
                        sortDojosByUserDistance(result) { dojosList -> listDojos.postValue(dojosList) }
                    }
                    else{
                        listDojos.postValue(result)
                    }
                }

                override fun onFailed(exception: Exception) {
                    TODO("Not yet implemented")
                }
            })
        }
        return listDojos
    }

    private fun getDistanceToUser(dojo: GeoPoint, index: Int): Double {
        if(userLocation.value != null)
        {
            val location = userLocation.value as Location
            val x = (location.latitude - dojo.latitude).pow(2)
            val y = (location.longitude - dojo.longitude).pow(2)
            return sqrt(x + y)
        }

        return index.toDouble()

    }

    fun sortDojosByUserDistance(dojos: List<Dojo>, callback: (ArrayList<Dojo>) -> Unit) {
        viewModelScope.launch {
            sortDojosByUserDistanceCoroutine(dojos, callback)
        }
    }

    private suspend fun sortDojosByUserDistanceCoroutine(
        dojos: List<Dojo>,
        callback: (ArrayList<Dojo>) -> Unit
    ) {
        withContext(Dispatchers.Default) {

            val arrayDojos = ArrayList<Dojo>(dojos)

            var currentDojo: Dojo?
            var currentDojoDistanceToUser: Double?
            var j: Int?
            for (i in 1 until arrayDojos.size) {
                currentDojo = arrayDojos[i]
                currentDojoDistanceToUser = getDistanceToUser(currentDojo.location, i)

                j = i
                while (j > 0 && currentDojoDistanceToUser < getDistanceToUser(arrayDojos[j - 1].location, j - 1)) {
                    arrayDojos[j] = arrayDojos[j - 1]
                    j -= 1
                }
                arrayDojos[j] = currentDojo
            }

            withContext(Dispatchers.Main) {
                //arrayDojos.forEach {  Log.e("NearbyDojosViewModel sortDojosByUserDistanceCoroutine", it.name)}
                //Log.e("NearbyDojosViewModel sortDojosByUserDistanceCoroutine", arrayDojos.toString())
                callback(arrayDojos)
            }
        }
    }

    private lateinit var dojoListClickCallback: (Int) -> Unit
    private lateinit var phoneClickCallback: (String) -> Unit

    fun registerDojoListClickCallback(callback: (Int) -> Unit){
        dojoListClickCallback = callback
    }

    fun registerPhoneClickCallback(callback: (String) -> Unit){
        phoneClickCallback = callback
    }

    fun callPhoneClickCallback(phone: String){
        Log.e("NearbyDojosViewModel callPhoneClickCallback", phone)
        phoneClickCallback(phone)
    }

    fun callDojoListClickCallback(index: Int){
        Log.e("NearbyDojosViewModel callDojoListClickCallback", index.toString())
        dojoListClickCallback(index)
    }

    companion object {
        @JvmStatic
        @BindingAdapter("loadImage")
        fun loadImage(imageView: ImageView, imageUrl: String?) {
            imageUrl?.let {
                if (it.isNotEmpty())
                    Glide.with(imageView.context).load(imageUrl).into(imageView)
            }
        }
    }
}