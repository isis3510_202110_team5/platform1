package com.moviles.kotlinmartialarts.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moviles.kotlinmartialarts.model.Channel
import com.moviles.kotlinmartialarts.model.Dojo
import com.moviles.kotlinmartialarts.model.Message
import com.moviles.kotlinmartialarts.model.User
import com.moviles.kotlinmartialarts.model.network.FirebaseCallback
import com.moviles.kotlinmartialarts.model.network.FirestoreServiceSingleton
import java.lang.Exception

class ChannelViewModel: ViewModel() {

    private val listChannels: MutableLiveData<List<Channel>> = MutableLiveData()

    fun getChannels() : LiveData<List<Channel>> {
        FirestoreServiceSingleton.getInstance().getChannels(object: FirebaseCallback<List<Channel>> {

            override fun onFailed(exception: Exception){

            }

            override fun onSuccess(result: List<Channel>?) {
                listChannels.postValue(result)
            }

        })


        return listChannels
    }

    fun setFavsMore(channel: String) {
        FirestoreServiceSingleton.getInstance().setChannelFavsMore(channel)

    }
    fun setFavsLess(channel: String) {
        FirestoreServiceSingleton.getInstance().setChannelFavsLess(channel)

    }

    private val user: MutableLiveData<User> = MutableLiveData()
    fun getUser(email:String) : LiveData<User> {
        FirestoreServiceSingleton.getInstance().getUserbyEmail(email,object:
                FirebaseCallback<List<User>> {
            override fun onFailed(exception: Exception) {
                TODO("Not yet implemented")
            }

            override fun onSuccess(result: List<User>?) {
                user.postValue(result?.get(0))
            }

        })

        return user
    }


}