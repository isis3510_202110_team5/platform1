package com.moviles.kotlinmartialarts.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.lang.NumberFormatException
import kotlin.properties.Delegates

class Channel{
    @DocumentId
    lateinit var documentId: String

    lateinit var name: String
    lateinit var description: String
    lateinit var img: String
    var msgs by Delegates.notNull<Long>()
}