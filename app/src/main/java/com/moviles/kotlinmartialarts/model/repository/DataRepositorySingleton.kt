package com.moviles.kotlinmartialarts.model.repository

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class DataRepositorySingleton private constructor() {

    companion object {
        private var instance: DataRepositorySingleton? = null

        fun getInstance(): DataRepositorySingleton {
            if (instance == null) {
                instance = DataRepositorySingleton()
            }
            return instance as DataRepositorySingleton
        }
    }

    private val userLocation: MutableLiveData<Location> = MutableLiveData()

    fun setLocation(newLocation: Location) {
        userLocation.postValue(newLocation)
    }

    fun getLocation(): LiveData<Location> {
        return userLocation
    }
}