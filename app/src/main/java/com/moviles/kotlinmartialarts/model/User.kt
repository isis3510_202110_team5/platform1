package com.moviles.kotlinmartialarts.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import kotlin.properties.Delegates

class User{
    @DocumentId
    lateinit var documentId: String

    lateinit var username: String
    lateinit var email: String
    lateinit var picture: String
    var age by Delegates.notNull<Long>()
    lateinit var martialarts:DocumentReference
}
