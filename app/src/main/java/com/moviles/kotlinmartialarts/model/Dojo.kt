package com.moviles.kotlinmartialarts.model

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.GeoPoint

class Dojo {
    lateinit var name: String
    lateinit var location: GeoPoint
    lateinit var image_url: String
    lateinit var address: String
    lateinit var city: String
    lateinit var region: String
    lateinit var country: String
    lateinit var phone: String
    lateinit var categories: List<DocumentReference>
}