package com.moviles.kotlinmartialarts.model.network

import com.moviles.kotlinmartialarts.model.User
import java.lang.Exception

interface FirebaseCallback<T> {
    fun onSuccess(result: T?)
    fun onFailed(exception: Exception)
}