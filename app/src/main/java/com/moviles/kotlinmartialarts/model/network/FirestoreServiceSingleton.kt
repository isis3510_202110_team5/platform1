package com.moviles.kotlinmartialarts.model.network

import android.util.Log
import com.google.common.net.InetAddresses.decrement
import com.google.firebase.firestore.*
import com.moviles.kotlinmartialarts.model.Channel
import com.moviles.kotlinmartialarts.model.Dojo
import com.moviles.kotlinmartialarts.model.Message
import java.util.*
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.moviles.kotlinmartialarts.model.User

const val DOJO_COLLECTION_NAME = "Dojo"
const val MESSAGES_COLLECTION_NAME = "messages"
const val USER_COLLECTION_NAME = "User"
const val CHANNEL_COLLECTION_NAME = "Channel"
const val TAG = "DEBUG"

class FirestoreServiceSingleton private constructor(){

    companion object {
        private var instance: FirestoreServiceSingleton? = null

        fun getInstance(): FirestoreServiceSingleton{
            if(instance == null){
                instance = FirestoreServiceSingleton()
            }
            return instance as FirestoreServiceSingleton
        }
    }

    private val firebaseFirestore = FirebaseFirestore.getInstance()
    private val settings = FirebaseFirestoreSettings.Builder().setPersistenceEnabled(true).build()

    init {
        firebaseFirestore.firestoreSettings = settings
    }

    fun getDojos(callback: FirebaseCallback<List<Dojo>>) {
        firebaseFirestore.collection(DOJO_COLLECTION_NAME)
            .get()
            .addOnSuccessListener { result ->
                for (doc in result){
                    val list = result.toObjects(Dojo::class.java)
                    result.forEach {
                        //Log.d("dojo", it.toString())
                    }
                    callback.onSuccess(list)
                    break
                }
            }
    }

    fun getChannels(callback: FirebaseCallback<List<Channel>>) {

        firebaseFirestore.collection(CHANNEL_COLLECTION_NAME)
                .orderBy("msgs", Query.Direction.DESCENDING)
                .get()
                .addOnSuccessListener { result ->

                    for (doc in result) {
                        val list = result.toObjects(Channel::class.java)
                        callback.onSuccess(list)

                        break
                    }
                }
    }



    fun setMessages(message: Message){

        firebaseFirestore.collection(MESSAGES_COLLECTION_NAME).document(message.timestamp)
                .set(message)
                .addOnSuccessListener{
                    Log.d(TAG, "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error writing document", e)
                }

    }

    fun setChannelMessage (channel:DocumentReference){
        firebaseFirestore.collection(CHANNEL_COLLECTION_NAME).document(channel.id)
                .update("msgs" , FieldValue.increment(1))
    }
    fun setChannelFavsMore (channel:String){
        firebaseFirestore.collection(CHANNEL_COLLECTION_NAME).document(channel)
                .update("favs" , FieldValue.increment(1))
    }
    fun setChannelFavsLess(channel:String){
        firebaseFirestore.collection(CHANNEL_COLLECTION_NAME).document(channel)
                .update("favs" , FieldValue.increment(-1))
    }
    fun getReference(reference: String): DocumentReference {
        val currentDocRef: DocumentReference = firebaseFirestore.document(reference)

        return currentDocRef

    }

    fun getIdChannel(name: String, callback:FirebaseCallback<Array<Any>>){

        firebaseFirestore.collection(CHANNEL_COLLECTION_NAME).whereEqualTo("name",name)
                .limit(1)
                .get()
                .addOnSuccessListener { result ->
                    result.forEach{
                       Log.d(TAG, "New Id Channel : ${it.id}")
                        val res: Array<Any> = arrayOf(it.id, it.metadata.isFromCache)
                        callback.onSuccess(res)
                    }
                }





    }

    fun getUserbyEmail(email: String, callback:FirebaseCallback<List<User>>){

        firebaseFirestore.collection(USER_COLLECTION_NAME).whereEqualTo("email",email)
                .limit(1)
                .get()
                .addOnSuccessListener { result ->
                    var list= result.toObjects(User::class.java)
                        callback.onSuccess(list)

                }

    }
    fun getUser(id: String, callback:FirebaseCallback<User>){
        firebaseFirestore.collection(USER_COLLECTION_NAME).
                document(id)
                .get()
                .addOnSuccessListener { result ->
                        val list = result.toObject(User::class.java)
                        callback.onSuccess(list)
                    }
                }





    fun getMessages(channel:DocumentReference,callback: FirebaseCallback<List<Message>>) {

        firebaseFirestore.collection(MESSAGES_COLLECTION_NAME)
                .addSnapshotListener{snapshot, e ->
            if (e != null) {
                Log.w(TAG, "listen:error", e);
                return@addSnapshotListener
            }
            for (dc in snapshot!!.documentChanges) {
            when (dc.type) {
                DocumentChange.Type.ADDED -> {
                    firebaseFirestore.collection(MESSAGES_COLLECTION_NAME)
                            .whereEqualTo("channel", channel)
                            .get()
                            .addOnSuccessListener { result ->
                                for (doc in result){
                                    val list = result.toObjects(Message::class.java)
                                    result.forEach {

                                    }
                                    callback.onSuccess(list)
                                    break
                                }
                            }

                    Log.d(TAG, "New message: ${dc.document.data}")
                }

            }
        }
    }


}
    fun add_user(firstName: String, lastName: String, email: String, username:String , password: String, Gender:String, city:String, state:String, country:String)
    {
        val user = hashMapOf(
            "firstName" to firstName,
            "lastName" to lastName,
            "email" to email,
            "username" to username,
            "password" to password,
            "gender" to Gender,
            "city" to city,
            "state" to state,
            "country" to country
        )

        firebaseFirestore.collection(USER_COLLECTION_NAME)
            .document(username).set(user).addOnSuccessListener { Log.d("USER", "user added succesfully")
                Log.d("USER", "username:" + username + '\n' + "email: " + email)
            }
            .addOnFailureListener{
                Log.d("USER", it.localizedMessage)
            }
    }

}

