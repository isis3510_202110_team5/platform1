package com.moviles.kotlinmartialarts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.google.firebase.auth.FirebaseAuth
import com.moviles.kotlinmartialarts.view.ui.activities.MainMenuActivity

const val RC_SIGN_IN = 123;

class MainActivity : AppCompatActivity() {

    private lateinit var fba: FirebaseAuth;
    private lateinit var email_input: TextView;
    private lateinit var password_input: TextView;
    private lateinit var log_in_button: Button
    private lateinit var sign_up_button: Button
    private lateinit var error_message_text: TextView
    private val receiver : BroadcastReceiver = NetworkReceiver();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Connectivity_check_singleton.connectivity_check(this)

        fba = FirebaseAuth.getInstance();
        if (checkIfAlreadyLoggedIn())
        {
            move_to_menu(fba.currentUser.email)
        }

        email_input = findViewById(R.id.email_input);
        password_input = findViewById(R.id.password_input);
        log_in_button = findViewById(R.id.login_button)
        sign_up_button = findViewById(R.id.signup_button)

        error_message_text = findViewById(R.id.textView_error_message);

        sign_up_button.setOnClickListener(){sign_up()}
        log_in_button.setOnClickListener(){sign_in()}
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("AUTH", "unregistered receiver")
        unregisterReceiver(receiver);
    }

    private fun checkIfAlreadyLoggedIn(): Boolean {
        return fba.currentUser != null
    }

    private fun sign_up() //TODO ITS ONLY CREATES THE USER IF THE EMAIL FOLLOWS THE EMAIL FORMAT, BUT DOES NOT LET THE USER KNOW IF HE DIDNT USE THAT FORMAT. FIX THAT.
    {
        move_to_username_confirmation_page()
    }

    private fun sign_in()
    {
        Connectivity_check_singleton.connectivity_check(this);

        Log.d("AUTH", "SIGN IN PRESIONADO")

        if(Connectivity_check_singleton.is_there_connection == true) //si hay internet
        {
            if (email_input.text.isNotEmpty() && password_input.text.isNotEmpty())
            {

                fba.signInWithEmailAndPassword(
                    email_input.text.toString(),
                    password_input.text.toString()
                )
                    .addOnSuccessListener {

                        Log.d("AUTH", "login exitoso")
                        Log.d("AUTH", (fba.currentUser != null).toString())
                        move_to_menu(email_input.text.toString())
                        print_error_message("")
                    }

                    .addOnFailureListener {
                        Log.d("AUTH", it.localizedMessage)

                        if (it.localizedMessage.contains("The password is invalid")) {
                            Log.d("AUTH", "contraseña o correo invalidos")
                            print_error_message("Wrong Email or Password")
                        } else if ((it.localizedMessage.contains("The email address is badly formatted"))) {
                            Log.d("AUTH", "correo invalido")
                            print_error_message("Invalid email, please check the input")
                        } else if (it.localizedMessage.contains("There is no user record")) {
                            Log.d("AUTH", "user not found")
                            print_error_message("Email not found. check email")
                        }
                    }
            }
            else
            {
                print_error_message("Please log in with your email and password")
            }
        }
        else
        {
            print_error_message("No internet connection found. Check your device´s configuration")
        }
    }

    private fun move_to_username_confirmation_page()
    {
        val UsernameIntent = Intent(this, UsernameConfirmationActivity::class.java)
        startActivity(UsernameIntent);
    }

    private fun move_to_menu(email:String)
    {
        val menuIntent = Intent(this, MainMenuActivity::class.java).apply {
            putExtra("email", email)
        }
        startActivity(menuIntent)
    }

    private fun print_error_message(string:String)
    {
        error_message_text.setText(string)
        error_message_text.setTextColor(getColor(R.color.red))
    }
}