package com.moviles.kotlinmartialarts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NetworkReceiver : BroadcastReceiver()
{

    override fun onReceive(context: Context, intent: Intent) {
        Log.d("AUTH","entro al onreceive")
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = cm.activeNetworkInfo;
        if(networkInfo?.type != ConnectivityManager.TYPE_WIFI && networkInfo?.type != ConnectivityManager.TYPE_MOBILE)
        {
            Log.d("AUTH","no hay internet")
            if(Connectivity_check_singleton.is_there_connection == true) //si antes habia conexion
            {
                Log.d("AUTH", "ya no hay internet")
                Toast.makeText(context,R.string.connection_lost, Toast.LENGTH_LONG).show() //ya no
                Connectivity_check_singleton.is_there_connection = false;
            }

        }
        else{
            if(Connectivity_check_singleton.is_there_connection == false) //si hubo un error de red antes
            {
                Toast.makeText(context,R.string.connection_found, Toast.LENGTH_LONG).show() //avisarle al usuario que se recupero la conexion
                Connectivity_check_singleton.is_there_connection = true;
            }
        }
    }
}