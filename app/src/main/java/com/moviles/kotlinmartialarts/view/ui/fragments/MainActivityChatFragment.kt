package com.moviles.kotlinmartialarts.view.ui.fragments

import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentReference
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.model.Message
import com.moviles.kotlinmartialarts.model.User
import com.moviles.kotlinmartialarts.viewmodel.MainViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.chat_item.view.*
import kotlinx.android.synthetic.main.main_activity_chat_activity.*
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.message_item.*
import kotlinx.android.synthetic.main.message_item.view.*
import java.util.*

class MainActivityChatFragment : Fragment() {

    companion object {
        fun newInstance() =
            MainActivityChatFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        this.arguments?.getString("user")?.let {
        viewModel.getUserMain(it).observe(viewLifecycleOwner, { users ->
            name.text=users.username
            try{
            Picasso.get()
                    .load(users.picture)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .fit()
                    .into(photo)
            }catch (e: Exception){

            }

        })
        }
        observeMessages()
        setupSendButton()
        setupButtonSpeak()
    }
    fun observeMessages() {
        this.arguments?.getString("channel")?.let {
            viewModel.getMessages(viewModel.document("Channel/"+it )).observe(viewLifecycleOwner, { messageList ->

                setupAdapter(messageList)
            })

        }

    }

    private fun setupAdapter(data: List<Message>) {
        val linearLayoutManager = LinearLayoutManager(this.context)
        viewMessages.layoutManager = linearLayoutManager
        viewMessages.adapter = ChatAdapter(this,data) {
          Toast.makeText(this.context, "${it.text} clicked", Toast.LENGTH_SHORT).show()

        }
        viewMessages.scrollToPosition(data.size - 1)
    }

    private fun setupSendButton() {
        btnSend.setOnClickListener {
            if (!txtMessage.text.toString().isEmpty()){
                sendData()
            }else{
                Toast.makeText(this.context, "Please enter a message", Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun setupButtonSpeak(){
        btnSpeak.setOnClickListener { speak() }
    }
    private val REQUEST_CODE_STT = 1
    fun speak(){
        var sttIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        sttIntent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        sttIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        sttIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak now!")

        try {
            ActivityCompat.startActivityForResult(this.activity!!,sttIntent, REQUEST_CODE_STT,null)

        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            Toast.makeText(this.context, "Your device does not support STT.", Toast.LENGTH_LONG).show()
        }
    }


    /**
     * Send data to firebase
     */
    private fun sendData(){

        lateinit var user: DocumentReference
        this.arguments?.getString("user")?.let {
                user =viewModel.document("User/"+it)
        }
        lateinit var channel: DocumentReference
        this.arguments?.getString("channel")?.let {
                channel =viewModel.document("Channel/"+ it)
        }

        var message= Message()
        message.text=(txtMessage.text.toString())
        message.channel=channel
        message.user=user
        message.timestamp= Date().time.toString()
        viewModel.setMessages(message)
        Toast.makeText(this.context, "Message sent", Toast.LENGTH_LONG).show()
        //clear the text
        txtMessage.setText("")




    }

}
