package com.moviles.kotlinmartialarts.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.moviles.kotlinmartialarts.BR
import com.moviles.kotlinmartialarts.viewmodel.NearbyDojosViewModel

class RecyclerNearbyDojosAdapter(private val nearbyDojosViewModel: NearbyDojosViewModel, private var resource: Int):
    RecyclerView.Adapter<RecyclerNearbyDojosAdapter.NearbyDojoHolder>() {

    private var itemCount: Int = 0

    fun setItemCount(itemCount: Int) {
        this.itemCount = itemCount
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NearbyDojoHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(p0.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(layoutInflater, p1, p0, false)
        return NearbyDojoHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemCount
    }

    override fun onBindViewHolder(p0: NearbyDojoHolder, p1: Int) {
        p0.setDataDojo(nearbyDojosViewModel, p1)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition()
    }

    private fun getLayoutIdForPosition(): Int {
        return resource
    }

    class NearbyDojoHolder(binding: ViewDataBinding): RecyclerView.ViewHolder(binding.root){
        private var binding: ViewDataBinding? = null

        init {
            this.binding = binding
        }

        fun setDataDojo(nearbyDojosViewModel: NearbyDojosViewModel, position: Int){
            binding?.setVariable(BR.model, nearbyDojosViewModel)
            binding?.setVariable(BR.position, position)
            binding?.executePendingBindings()
        }

    }
}