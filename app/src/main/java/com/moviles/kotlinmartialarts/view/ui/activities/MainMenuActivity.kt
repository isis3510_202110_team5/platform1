package com.moviles.kotlinmartialarts.view.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.databinding.ActivityMainMenuBinding
import com.moviles.kotlinmartialarts.view.ui.fragments.SetChatsActivity

class MainMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        val activityMainMenuBinding: ActivityMainMenuBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main_menu)
        activityMainMenuBinding.mergeMainMenuCardChannels.cvMainMenuChannels
            .setOnClickListener { intentChatActivity() }
        activityMainMenuBinding.mergeMainMenuCardNearbydojos.cvMainMenuNearbyDojos
            .setOnClickListener { intentNearbyDojosActivity() }
    }

    private fun intentNearbyDojosActivity() {
        val mapIntent = Intent(this, MapActivity::class.java).apply {
            //putExtra("email", email)
        }
        startActivity(mapIntent);
    }

    private fun intentChatActivity() {
        val chatIntent = Intent(this, SetChatsActivity::class.java).apply {
            putExtra("email", intent.getStringExtra("email").toString())
        }
        startActivity(chatIntent);
    }
}