package com.moviles.kotlinmartialarts.view.ui.fragments
import android.text.format.DateUtils
import android.view.LayoutInflater
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.model.Message
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.auth.data.model.User.getUser
import com.moviles.kotlinmartialarts.model.User
import com.moviles.kotlinmartialarts.viewmodel.MainViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.chat_item.view.*
import kotlinx.android.synthetic.main.message_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class ChatAdapter(val frag: Fragment, val messages: List<Message>, val itemClick: (Message) -> Unit) :
    RecyclerView.Adapter<ChatAdapter.ViewHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false)
        
        return ViewHolder(view, itemClick)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindForecast(messages[position],frag)
    }



    override fun getItemCount() = messages.size

    class ViewHolder(view: View,val itemClick: (Message) -> Unit) : RecyclerView.ViewHolder(view) {

    private lateinit var viewModel: MainViewModel

        fun bindForecast(message: Message,frag:Fragment) {

        viewModel = ViewModelProvider(frag).get(MainViewModel::class.java)


            with(message) {

                viewModel.getUser(message.user.id).observe(frag.viewLifecycleOwner, { users ->
                    if (users != null){
                        Picasso.get()
                                .load(users.picture)
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.error)
                                .fit()
                                .into(itemView.imageViewMsgs)
                    }
                })


                itemView.messageAdapterMessageItem.text =message.text
                itemView.messageAdapterName.text= message.user.id
                itemView.messageAdapterTime.text= fromMillisToTimeString(java.lang.Long.valueOf(message.timestamp))
                itemView.setOnClickListener { itemClick(this) }
            }
        }
        private fun fromMillisToTimeString(millis: Long) : String {
            val format = SimpleDateFormat("hh:mm a", Locale.getDefault())
            return format.format(millis)
        }

    }
}
