package com.moviles.kotlinmartialarts.view.ui.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.viewmodel.NearbyDojosViewModel


class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var viewModel: NearbyDojosViewModel
    private lateinit var mapMarkers: ArrayList<MarkerOptions>
    private var googleMap: GoogleMap? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(requireActivity()).get(NearbyDojosViewModel::class.java)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        registerDojoListClickCallback()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        observeDojos()
    }

    private fun observeDojos() {
        viewModel.getDojos().observe(viewLifecycleOwner, { dojoList ->
            mapMarkers = ArrayList()
            dojoList.forEach { dojo ->
                mapMarkers.add(
                    buildMarker(
                        LatLng(dojo.location.latitude, dojo.location.longitude),
                        dojo.name,
                        dojo.image_url
                    )
                )
            }
            if (mapMarkers.isNotEmpty()) {
                setMapFocus(mapMarkers[0])
            }
        })
    }

    private fun buildMarker(position: LatLng, name: String, imageUrl: String): MarkerOptions {
        val markerOptions = MarkerOptions()
        markerOptions.position(position)
        markerOptions.title(name)

        Glide.with(context!!).asBitmap().load(imageUrl)
            .into(object : CustomTarget<Bitmap>(100, 100) {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val smallMarker = Bitmap.createScaledBitmap(
                        resource,
                        150,
                        150,
                        false
                    )
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap((smallMarker)))
                    googleMap?.addMarker(markerOptions)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    TODO("Not yet implemented")
                }
            })


        return markerOptions
    }

    private fun setMapFocus(marker: MarkerOptions) {
        val zoom = 16f
        googleMap?.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                marker.position,
                zoom
            )
        )
    }

    private fun registerDojoListClickCallback(){
        viewModel.registerDojoListClickCallback { dojoIndex ->
            setMapFocus(mapMarkers[dojoIndex])
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        TODO("Not yet implemented")
    }
}