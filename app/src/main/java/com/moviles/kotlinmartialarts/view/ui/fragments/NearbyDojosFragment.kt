package com.moviles.kotlinmartialarts.view.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.databinding.FragmentNearbyDojosBinding
import com.moviles.kotlinmartialarts.model.Dojo
import com.moviles.kotlinmartialarts.viewmodel.NearbyDojosViewModel

class NearbyDojosFragment : Fragment() {

    private lateinit var viewModel: NearbyDojosViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val fragmentNearbyDojosFragmentBinding: FragmentNearbyDojosBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_nearby_dojos, container, false)
        viewModel = ViewModelProvider(requireActivity()).get(NearbyDojosViewModel::class.java)
        fragmentNearbyDojosFragmentBinding.model = viewModel
        return fragmentNearbyDojosFragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListUpdate()
        registerPhoneClickCallback()
    }

    private fun setupListUpdate() {
        viewModel.getDojos().observe(viewLifecycleOwner, { dojos: List<Dojo> ->
                viewModel.setDojosInRecyclerAdapter(dojos.size)
        })
    }

    private fun registerPhoneClickCallback(){
        viewModel.registerPhoneClickCallback { phone ->
            val intent = Intent(Intent.ACTION_DIAL).apply{
                data = Uri.parse("tel:${phone}")
            }
            startActivity(intent)
        }
    }
}