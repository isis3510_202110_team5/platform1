package com.moviles.kotlinmartialarts.view.ui.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.model.Channel
import com.moviles.kotlinmartialarts.model.User
import com.moviles.kotlinmartialarts.viewmodel.ChannelViewModel
import kotlinx.android.synthetic.main.activity_set_chats.*
import kotlinx.android.synthetic.main.chat_item.*
import kotlinx.android.synthetic.main.main_activity_chat_activity.*

class SetChatsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var viewModel: ChannelViewModel
    private val key ="Favorites"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_set_chats)

        viewModel = ViewModelProvider(this).get(ChannelViewModel::class.java)
        tabLayout.setOnClickListener{
            Toast.makeText(this,(tabLayout.selectedTabPosition.toString()),Toast.LENGTH_SHORT).show()

        }
        val prefs= PreferenceManager.getDefaultSharedPreferences(this)

        //TabNew.setOnClickListener(this)
        observeChats(prefs)
    }

    fun observeChats(prefs: SharedPreferences) {
        val email=intent.getStringExtra("email").toString()
        viewModel.getChannels().observe(this, Observer { channelList ->


            viewModel.getUser(email).observe(this, Observer { user ->
                setupAdapter(prefs,channelList,user)
            })



        })
    }
    private fun setupAdapter(prefs:SharedPreferences,data: List<Channel>,users: User) {


        val l = LinearLayoutManager(this)
        viewChats.layoutManager = l
        nameChannelPopular.text= data[0].name

        viewChats.adapter = ListChatAdapter(this,prefs,users,data) {
            Toast.makeText(this, "${it.name} clicked", Toast.LENGTH_SHORT).show()
        }
        viewChats.scrollToPosition(data.size - 1)
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }


}