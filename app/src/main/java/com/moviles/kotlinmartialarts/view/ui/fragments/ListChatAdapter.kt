package com.moviles.kotlinmartialarts.view.ui.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.moviles.kotlinmartialarts.MainActivityChat
import com.moviles.kotlinmartialarts.R
import com.moviles.kotlinmartialarts.model.Channel
import com.moviles.kotlinmartialarts.model.User
import com.moviles.kotlinmartialarts.viewmodel.ChannelViewModel
import com.moviles.kotlinmartialarts.viewmodel.MainViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.chat_item.view.*


class ListChatAdapter(val frag: AppCompatActivity, val prefs: SharedPreferences, val user: User, val chats: List<Channel>, val itemClick: (Channel) -> Unit) :
        RecyclerView.Adapter<ListChatAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.chat_item, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindForecast(prefs, user, chats[position],frag)

    }



    override fun getItemCount() = chats.size

    class ViewHolder(view: View, val itemClick: (Channel) -> Unit) : RecyclerView.ViewHolder(view), View.OnClickListener {
        lateinit var context : Context
        lateinit var emails : User
        lateinit var channels : Channel
        private lateinit var viewModel: ChannelViewModel

        fun bindForecast(prefs: SharedPreferences, email: User, channel: Channel,frag: AppCompatActivity) {
            viewModel = ViewModelProvider(frag).get(ChannelViewModel::class.java)
            emails = email
            channels=channel
            context= itemView.getContext()
            itemView.cardChat.setOnClickListener(this)
            var objects = prefs.getStringSet("Favorites", null)
            if (objects != null) {
                if(objects.contains(channel.documentId)){
                    itemView.imageButton.setImageResource(R.drawable.ic_baseline_favorite_24)
                }
                else{
                    itemView.imageButton.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                }
            }
            itemView.imageButton.setOnClickListener{


                val edit =prefs.edit()
                var objetos = prefs.getStringSet("Favorites", null)
                if (objetos != null) {
                    if(objetos.contains(channel.documentId)){
                            itemView.imageButton.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                            objetos.remove(channel.documentId)
                            viewModel.setFavsLess(channel.documentId)
                        }
                        else{
                            itemView.imageButton.setImageResource(R.drawable.ic_baseline_favorite_24)
                            objetos.add(channel.documentId)
                            viewModel.setFavsMore(channel.documentId)
                            edit.remove("Favorites")
                            edit.apply()
                            edit.putStringSet("Favorites", objetos)
                            edit.apply()
                                            }
                }else{
                    itemView.imageButton.setImageResource(R.drawable.ic_baseline_favorite_24)
                    val set: Set<String> = setOf(channel.documentId)
                    viewModel.setFavsMore(channel.documentId)
                    edit.putStringSet("Favorites",set)
                    edit.apply()

                }
                


            }
            with(channel) {
                itemView.chatAdapterName.text =channel.name
                itemView.textDescription.text=channel.description

                Picasso.get()
                    .load(channel.img)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .fit()
                    .into(itemView.imageView)
            }

        }

        override fun onClick(v: View?) {

            val chatIntent = Intent(context, MainActivityChat::class.java).apply {
                putExtra("user", emails.documentId)
                putExtra("channel", channels.documentId)
            }
            context.startActivity(chatIntent);
        }
    }
}




