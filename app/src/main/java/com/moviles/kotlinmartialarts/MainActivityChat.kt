package com.moviles.kotlinmartialarts

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import androidx.appcompat.app.AppCompatActivity
import com.moviles.kotlinmartialarts.view.ui.fragments.MainActivityChatFragment
import kotlinx.android.synthetic.main.chat_item.view.*
import kotlinx.android.synthetic.main.main_fragment.*

class MainActivityChat : AppCompatActivity() {
    var extra=""
    var email=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_chat_activity)
        extra= intent.getStringExtra("channel").toString()
        email= intent.getStringExtra("user").toString()

        val args = Bundle()
        args.putString("channel", extra)
        args.putString("user",email)

        if (savedInstanceState == null) {
            val newFragment = MainActivityChatFragment()
            newFragment.setArguments(args)
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, newFragment)
                    .commitNow()
        }
    }
    override fun onActivityResult(requestCode: Int,
                                  resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    result?.let {
                        val recognizedText = it[0]
                        txtMessage.setText(recognizedText)
                    }
                }
            }
        }

    }
}