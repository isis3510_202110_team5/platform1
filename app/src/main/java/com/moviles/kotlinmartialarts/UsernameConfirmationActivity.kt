package com.moviles.kotlinmartialarts

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.moviles.kotlinmartialarts.view.ui.activities.MainMenuActivity
import com.moviles.kotlinmartialarts.viewmodel.UserViewModel


class UsernameConfirmationActivity : AppCompatActivity() {

    private lateinit var fba: FirebaseAuth

    lateinit var first_name_input: TextView
    lateinit var last_name_input: TextView
    lateinit var email_adress_input: TextView
    lateinit var username: TextView
    lateinit var password_input: TextView
    lateinit var gender_input: TextView
    lateinit var city_input: TextView
    lateinit var state_input: TextView
    lateinit var country_input: TextView

    lateinit var error_message_sign_up : TextView

    lateinit var confirm_button: TextView
    lateinit var cancel_button : TextView
    lateinit var viewmodel: UserViewModel

    lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup_activity);

        Connectivity_check_singleton.connectivity_check(this);

        fba = FirebaseAuth.getInstance()

        first_name_input = findViewById(R.id.tiSignupMenuFirstName)
        last_name_input = findViewById(R.id.tiSignupMenuLastName)
        email_adress_input = findViewById(R.id.tiSignupMenuEmail)
        username = findViewById(R.id.tiSignupMenuUsername)
        password_input = findViewById(R.id.tiSignupMenuPassword)
        gender_input = findViewById(R.id.tiSignupMenuGender)
        city_input = findViewById(R.id.tiSignupMenuCity)
        state_input = findViewById(R.id.tiSignupMenuRegion)
        country_input = findViewById(R.id.tiSignupMenuCountry)

        error_message_sign_up = findViewById(R.id.error_message_sign_up);

        confirm_button = findViewById(R.id.sign_up_confirm_button)
        cancel_button = findViewById(R.id.cancel_button_sign_up);

        confirm_button.setOnClickListener(){confirm_button_pressed()}
        cancel_button.setOnClickListener() { move_to_log_in() }

        viewmodel = ViewModelProvider(this).get(UserViewModel::class.java)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
    }

    private fun confirm_button_pressed() //this function adds the username to the database and links an email to it.
    {
        Connectivity_check_singleton.connectivity_check(this);

        Log.d("AUTH", "SIGN IN PRESIONADO")

        if(Connectivity_check_singleton.is_there_connection == true) //si hay internet
        {
            if(check_every_field_not_empty()) //si todos los campos ya esta lleno
            {
                //registrar el usuario
                fba.createUserWithEmailAndPassword(email_adress_input.text.toString(), password_input.text.toString()).addOnSuccessListener {
                    viewmodel.add_user(first_name_input.text.toString(),last_name_input.text.toString(), email_adress_input.text.toString(), username.text.toString(),password_input.text.toString(), gender_input.text.toString(), city_input.text.toString(), state_input.text.toString(), country_input.text.toString())

                    val editor:SharedPreferences.Editor = prefs.edit()
                    editor.putString("city", city_input.text.toString())
                    editor.putString("region", state_input.text.toString())
                    editor.putString("country", country_input.text.toString())
                    editor.commit()

                    move_to_menu();
                } .addOnFailureListener {
                    Log.d("AUTH", it.localizedMessage)
                    if(it.localizedMessage.contains("email address is already in use"))
                    {
                        print_error_message("email already taken")
                    }
                    else if ((it.localizedMessage.contains("The email address is badly formatted"))) {
                        print_error_message("Invalid email, please check the input")
                    }
                    else if (it.localizedMessage.contains("Password should be at least 6 characters"))
                    {
                        print_error_message("Password should be at least 6 characters")
                    }
                }
            }
            else {
                print_error_message("please make sure you fill every field above")
            }
        }
        else
        {
            print_error_message("No internet connection found. Check your device´s configuration")
        }
    }

    private fun move_to_menu()
    {
        val mapIntent = Intent(this, MainMenuActivity::class.java)
        startActivity(mapIntent);
    }

    private fun move_to_log_in()
    {
        val loginintent = Intent(this, MainActivity::class.java)
        fba.signOut()
        startActivity(loginintent)
    }

    private fun print_error_message(string:String)
    {
        error_message_sign_up.setText(string)
        error_message_sign_up.setTextColor(getColor(R.color.red))
    }

    private fun check_every_field_not_empty():Boolean //true si todos los campos estan lleno
    {
        return (first_name_input.text.toString().isNotEmpty() &&
        last_name_input.text.toString().isNotEmpty() &&
        email_adress_input.text.toString().isNotEmpty() &&
        username.text.toString().isNotEmpty() &&
        password_input.text.toString().isNotEmpty() &&
        gender_input.text.toString().isNotEmpty() &&
        city_input.text.toString().isNotEmpty() &&
        state_input.text.toString().isNotEmpty() &&
        country_input.text.toString().isNotEmpty())
    }
}