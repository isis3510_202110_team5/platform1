package com.moviles.kotlinmartialarts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log

object Connectivity_check_singleton
{
    private val receiver : BroadcastReceiver = NetworkReceiver()
    private var init_called_before: Boolean = false
    var is_there_connection : Boolean = true; //TRUE SI HAY CONEXION, FALSE SI NO

    init {
        Log.d("CONNECTIVITY", "Singleton initialized")
    }

    private fun initWith(context: Context)
    {
        val filter = IntentFilter().apply { addAction("com.moviles.kotlinmartialarts.CONNECTIVITY_CHECK") }
        context.registerReceiver(receiver, filter)
        init_called_before = true;
        Log.d("AUTH", "registered receiver")
    }

    fun connectivity_check(context: Context) //llamar esto para ver si hay internet o no. Despues de llamarlo, revisar is_there_conection
    {
        if (init_called_before == false)
        {
            initWith(context);
        }
        Intent().also { intent ->
            intent.action = "com.moviles.kotlinmartialarts.CONNECTIVITY_CHECK"
            context.sendBroadcast(intent)
        }
    }


}